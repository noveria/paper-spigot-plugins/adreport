package me.linuxsquare.adreport.command

import me.linuxsquare.adreport.AdReport
import me.linuxsquare.adreport.models.LanguageModel.Companion.send
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class AdReportCommand(private val adReport: AdReport): CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>?): Boolean {
        if (args?.size == 0 || args == null) {
            adReport.languagemodel.getMessage(if (sender is Player) adReport.languagemodel.getLocale(sender.uniqueId)
            else adReport.configmodel.config.getString("Settings.fallback")!!,
                "base.argumentRequired",
                "§c").send(sender)
            return true
        }
        when (args[0].lowercase()) {
            "reload" -> {
                if(sender.isOp || sender.hasPermission("adreport.reload")) {
                    adReport.configmodel.reload()
                    adReport.languagemodel.reload()
                    adReport.languagemodel.getMessage(if (sender is Player) adReport.languagemodel.getLocale(sender.uniqueId)
                    else adReport.configmodel.config.getString("Settings.fallback")!!,
                        "base.reloaded",
                        "§a").send(sender)
                } else {
                    adReport.languagemodel.getMessage(if(sender is Player) adReport.languagemodel.getLocale(sender.uniqueId)
                    else adReport.configmodel.config.getString("Settings.fallback")!!,
                        "base.unsufficientPermission",
                        "§c").send(sender)
                }
            }
            else -> {
                if(sender.isOp || sender.hasPermission("adreport.execute")) {
                    val offlinePlayer: OfflinePlayer? = if (Bukkit.getPlayerUniqueId(args[0]) != null) Bukkit.getOfflinePlayer(Bukkit.getPlayerUniqueId(args[0])!!) else null
                    if(offlinePlayer != null) {
                        if(offlinePlayer.player?.name == sender.name) {
                            return true
                        }
                        if(!didPlayerAdvert(args[0], args[1])) {
                            adReport.languagemodel.getMessage(if (sender is Player) adReport.languagemodel.getLocale(sender.uniqueId)
                            else adReport.configmodel.config.getString("Settings.fallback")!!,
                                "execution.noadvert",
                                "§c").send(sender)
                            return true
                        }
                        adReport.LOG.severe("${args[0]} has adverted for a different server: ${args[1]}")
                        adReport.server.dispatchCommand(adReport.server.consoleSender, "${adReport.configmodel.config.getString("Settings.defaultExecution")} ${args[0]} ${adReport.languagemodel.getRawMessage(adReport.languagemodel.getLocale(offlinePlayer.uniqueId), "execution.message", "§c")}")
                    }
                } else {
                    adReport.languagemodel.getMessage(if(sender is Player) adReport.languagemodel.getLocale(sender.uniqueId)
                    else adReport.configmodel.config.getString("Settings.fallback")!!,
                        "base.unsufficientPermission",
                        "§c").send(sender)
                }
            }
        }
        return false
    }

    private fun didPlayerAdvert(playerName: String, domain: String): Boolean {

        val logFile = File("logs/latest.log")
        val domainRegex = "\\b((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,63}\\b".toRegex()
        val messageRegex = "<\\w+>".toRegex()

        BufferedReader(FileReader(logFile)).use { reader ->
            var line = reader.readLine()
            while (line != null) {
                if (domainRegex.matches(domain) && messageRegex.find(line) != null) {
                    if (line.contains(playerName) && line.lowercase().contains(domain.lowercase())) {
                        return true
                    }
                }
                line = reader.readLine()
            }
        }
        return false
    }
}