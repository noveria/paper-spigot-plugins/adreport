package me.linuxsquare.adreport.models

import me.linuxsquare.adreport.AdReport
import org.bukkit.configuration.file.FileConfiguration
import java.io.File

class ConfigModel(private val adReport: AdReport) {

    var config: FileConfiguration
        get() = field

    private lateinit var pluginFolder: File
    private lateinit var configFile: File

    init {
        this.config = adReport.config
    }

    fun loadConfig() {
        pluginFolder = File("plugins${System.getProperty("file.separator")}${adReport.description.name}")
        if(!pluginFolder.exists()) {
            pluginFolder.mkdir()
        }

        configFile = File("$pluginFolder${System.getProperty("file.separator")}config.yml")
        if(!configFile.exists()) {
            adReport.saveDefaultConfig()
        }
    }

    fun reload() {
        configFile = File("$pluginFolder${System.getProperty("file.separator")}config.yml")
        config.load(configFile)
    }

}