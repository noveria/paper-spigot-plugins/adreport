package me.linuxsquare.adreport.models

import me.linuxsquare.adreport.AdReport
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files
import java.util.*

class LanguageModel(private val adReport: AdReport) {

    /**
     * IMPORTANT: Add new language-files to this array, for dynamic creation
     */
    private val langfilenames: MutableList<String> = mutableListOf("en_gb", "de_ch")

    private val localeSettings = mutableMapOf<UUID, String>()
    private val messages: MutableMap<String, MutableMap<String, String>>  = mutableMapOf()

    fun getMessage(locale: String, pathToMessage: String): String {
        val langFile = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales", "$locale.yml")
        return "${adReport.PREFIX}${markdownifyMessage(ChatColor.stripColor(messages[langFile.name]?.get(pathToMessage)), "§r")}"
    }

    fun getMessage(locale: String, pathToMessage: String, colorCode: String): String {
        val langFile = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales", "$locale.yml")
        return "${adReport.PREFIX}${markdownifyMessage(ChatColor.stripColor(messages[langFile.name]?.get(pathToMessage)), colorCode)}"
    }

    fun getRawMessage(locale: String, pathToMessage: String): String {
        val langFile = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales", "$locale.yml")
        return markdownifyMessage(ChatColor.stripColor(messages[langFile.name]?.get(pathToMessage)), "§r")
    }

    fun getRawMessage(locale: String, pathToMessage: String, colorCode: String): String {
        val langFile = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales", "$locale.yml")
        return markdownifyMessage(ChatColor.stripColor(messages[langFile.name]?.get(pathToMessage)), colorCode)
    }

    private fun markdownifyMessage(input: String?, colorCode: String): String {
        if(input == null) {
            return ""
        }
        val pattern = Regex("(\\*\\*|__|\\*|_|~~|\\*\\*\\*)(.*?)(\\1)")
        val replaced = pattern.replace(input) {
            val text = it.groupValues[2]
            when(it.groupValues[1]) {
                "**" -> "§l$text$colorCode"
                "__" -> "§l$text$colorCode"
                "*" -> "§o$text$colorCode"
                "_" -> "§o$text$colorCode"
                "~~" -> "§m$text$colorCode"
                "***" -> "§l§o$text$colorCode"
                else -> text
            }
        }
        return "${colorCode}${replaced}"
    }

    companion object {
        fun String.send(sender: CommandSender) {
            sender.sendMessage(this)
        }
    }

    fun getLocale(uuid: UUID): String {
        if(localeSettings[uuid].isNullOrEmpty()) {
            return if(langfilenames.contains(adReport.configmodel.config.getString("Settings.fallback")!!)) {
                adReport.configmodel.config.getString("Settings.fallback")!!
            } else {
                "en_gb"
            }
        }
        return "${localeSettings[uuid]}"
    }

    fun setLocale(uuid: UUID, locale: String) {
        val file = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales", "$locale.yml")

        if(!file.exists()) {
            if(langfilenames.contains(adReport.configmodel.config.getString("Settings.fallback")!!)) {
                localeSettings[uuid] = adReport.configmodel.config.getString("Settings.fallback")!!
            } else {
                adReport.LOG.severe("Defined fallback-language '${adReport.configmodel.config.getString("Settings.fallback")!!}' in config.yml doesn't exist! " +
                        "Defaulting to 'en_gb'!")
                localeSettings[uuid] = "en_gb"
            }
        } else {
            localeSettings[uuid] = locale
        }
    }

    fun removePlayer(uuid: UUID) {
        localeSettings.remove(uuid)
    }

    fun loadMessages() {
        val langFolder = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales")
        if(!langFolder.exists()) {
            langFolder.mkdir()
        }

        /**
         * Dynamic file creation
         */
        for(filename: String in langfilenames) {
            val file = File(langFolder, "$filename.yml")
            try {
                if(file.exists()) {
                   Files.delete(file.toPath())
                }
                val input: InputStream = adReport.getResource("locale${System.getProperty("file.separator")}$filename.yml")
                    ?: throw FileNotFoundException("File $filename.yml not found in code! Please contact the maintainer of the plugin")

                Files.copy(input, file.toPath())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        if(langFolder.listFiles() == null) {
            throw FileNotFoundException("No files found in /locale directory!")
        }

        for(file: File in langFolder.listFiles()!!) {
            val localeMessages = mutableMapOf<String,String>()
            val lang: FileConfiguration = YamlConfiguration.loadConfiguration(file)
            for(key:String in lang.getKeys(false)) {
                for(messName: String in lang.getConfigurationSection(key)?.getKeys(false)!!) {
                    val message = ChatColor.translateAlternateColorCodes('&', lang.getString("$key.$messName")!!)
                    localeMessages["$key.$messName"] = message
                }
            }
            messages[file.name] = localeMessages
        }
    }

    fun reload() {
        val langFolder = File("${adReport.dataFolder}${System.getProperty("file.separator")}locales")
        if(langFolder.listFiles() == null) {
            throw FileNotFoundException("No files found in /locale directory!")
        }

        for(file: File in langFolder.listFiles()!!) {
            val localeMessages = mutableMapOf<String,String>()
            val lang: FileConfiguration = YamlConfiguration.loadConfiguration(file)
            for(key:String in lang.getKeys(false)) {
                for(messName: String in lang.getConfigurationSection(key)?.getKeys(false)!!) {
                    val message = ChatColor.translateAlternateColorCodes('&', lang.getString("$key.$messName")!!)
                    localeMessages["$key.$messName"] = message
                }
            }
            messages[file.name] = localeMessages
        }
    }

}