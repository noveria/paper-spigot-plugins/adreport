package me.linuxsquare.adreport.listener

import me.linuxsquare.adreport.AdReport
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerLocaleChangeEvent
import org.bukkit.event.player.PlayerQuitEvent

class PlayerListener(private val adReport: AdReport): Listener {

    @EventHandler
    fun onPlayerLeave(e: PlayerQuitEvent) {
        adReport.languagemodel.removePlayer(e.player.uniqueId)
    }

    @EventHandler
    fun onPlayerChangeLocale(e: PlayerLocaleChangeEvent) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(adReport, {
            adReport.languagemodel.removePlayer(e.player.uniqueId)
            adReport.languagemodel.setLocale(e.player.uniqueId, e.player.locale().toString().lowercase())
        }, 20)
    }

}